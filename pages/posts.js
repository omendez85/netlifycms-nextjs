import { useRouter } from 'next/router'

const Posts = () => {
    const router = useRouter()
    const { pid } = router.query
    const content = import(`../_posts/blog/my-post.md`)
    console.log(content)
    return <p>Post: {pid}</p>
}

export default Posts