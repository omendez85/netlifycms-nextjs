import Head from "next/head"
import { useRouter } from 'next/router'

import { Component } from 'react'
import { attributes, react as HomeContent } from '../content/home.md';

export default class Home extends Component {
    render() {
        let { title, topnews } = attributes;
        return (
            <>
               
                <article>
                    <h1>{title}</h1>
                    <HomeContent />
                    <ul>
                        {topnews.map((news, k) => (
                            <li key={k}>
                                <h2>{news.name}</h2>
                                <span>{news.state}</span>
                                <a href={news.link}>{news.name}</a>
                            </li>
                        ))}
                    </ul>
                </article>
            </>
        )
    }
}